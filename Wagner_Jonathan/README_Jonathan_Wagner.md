Jonathan Wagner

# Questions TP VLC++

## 4e étape : photos et videos
La methode play implémenté dans la classe de base (Media) est une methode abstraite.
On les déclare virtual et on ajoute à la fin de la declaration : 

` = 0 `

## 5e étape : traitement générique
La propriété caractéristique de l'orienté objet qui permet de faire *cela* est le polymorphisme.

Dans le cas du C++ il faut absolument un compteur qui permet de savoir combien il y a d'élément dans le tableau.

Les éléments du tableau sont des Media * (classe de base)
Il s'agit de pointeurs vers les objets et non des objets eux-même (contrairement à Java)

## 6e étape : Films et tableaux
Pour avoir le controle sur les données de l'objet film, lorsque l'on reçoit le tableau de de durée des chapitres à l'initialisation, il faut copier chacune des valeurs et non pas récupérer le pointeur. De cette façon les chapitres du film n'ont plus aucun lien avec la liste utilisé pour l'initialiser.

De plus avec un prototype de ce type :     

	` const int * getChapters() const {return chapters;} `

On s'assure que la fonction getChapters ne modifie pas l'objet qui l'appelle (grace au second const) ET que la valeur retournée ne pourra pas non plus être modifié (grace au premier const)
J'ai bien vérifié que si je suprimme la liste de chapitre utilisée pour l'instanciation, l'appelle de la fonction printChapters ne pose pas de problème.

## 7e étape : Destruction et copie des objets
Les classes qui sont concernées par la "fuite mémoire" sont :
 - Film
 
Il faut modifier le destructeur de la classe Film en y rajoutant :
 
    ` delete[] chapters `
 
En ce qui concerne la copie d'objet, la méthode setChapters qui est appelé à chaque fois, doit dans un premier temps appeler ` delete[] chapters ` avant d'y mettre une valeur !
 
## 8e étape : Créer des groupes
La classe groupe n'a pas de new, donc pas besoin de destructeur.
La seul utilité est pour le debug / developpement pour ajouter un output et verifier que le groupe est bien supprimé au moment ou l'on s'y attend.
La liste d'objet est en fait une liste de pointeurs pour ne pas que les objets soient détruit lorsque la liste est supprimée.
 
En java on ne s'embete jamais avec ça, le garbage collector s'assure de faire le menage.
 
## 9e étape : Gestion automatique de la mémoire
Pas de questions
 
## étape 10 : Gestion cohérente des données
Pour empecher la création d'objets de type Media ou de groupe autrement qu'avec la MediaFactory, on déclare leur constructeur comme private (ou protected) et on déclare la classe Mediafactory comme friend. 

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by john on 25/10/2016.
 */
public class RemoteControl extends JFrame {

    private boolean DEBUG = true;

    private static final long serialVersionUID = 1L;

    private CustomAction cAction1;
    private CustomAction cAction2;
    private CustomAction cActionClose;

    private JButton button1;
    private JButton button2;
    private JButton buttonClose;
    private JTextArea textArea;

    // Coonexion with the C++ server
    static final String DEFAULT_HOST = "localhost";
    static final int DEFAULT_PORT = 3331;
    private Socket sock;
    private BufferedReader input;
    private BufferedWriter output;

    public static void main(String argv[ ]) {


        // ########

        String host = DEFAULT_HOST;
        int port = DEFAULT_PORT;
        if (argv.length >=1) host = argv[0];
        if (argv.length >=2) port = Integer.parseInt(argv[1]);

        Client client = null;

        try {
            client = new Client(host, port);
        }
        catch (Exception e) {
            System.err.println("Client: Couldn't connect to "+host+":"+port);
            System.exit(1);
        }

        System.out.println("Client connected to "+host+":"+port);


        RemoteControl topLevel = new RemoteControl(client);
        topLevel.getContentPane().setLayout(new BorderLayout());


        // pour lire depuis la console
//        BufferedReader cin = new BufferedReader(new InputStreamReader(System.in));

//        while (true) {
//            System.out.print("Request: ");
//            try {
//                String request = cin.readLine();
//                String response = client.send(request);
//                System.out.println("Response: " + response);
//            }
//            catch (java.io.IOException e) {
//                System.err.println("Client: IO error");
//                return;
//            }
//        }


    }

    public RemoteControl(Client client){
        printDebug("Initialisation...");
        printDebug("Creating buttons...");

        //Create the actions
        cAction1 = new CustomAction("Search",client);
        cAction2 = new CustomAction("Play",client);
        cActionClose = new CustomAction("Close",client);

        //Create the buttons
        button1 = new JButton(cAction1);
        button2 = new JButton(cAction2);
        buttonClose = new JButton(cActionClose);

        //Create a JPanel and put the buttons in it
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));
        panel.add(button1);
        panel.add(button2);
        panel.add(buttonClose);

        //Position the panel in th south
        add(panel, BorderLayout.SOUTH);

        //Create a text Area and put it in a scrollPane in the center
        textArea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setPreferredSize(new Dimension(400, 300));

        //Position the panel in the center
        add(scrollPane,BorderLayout.CENTER);

        //Jmenu
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = new JMenu("File");
        menu.add(new JMenuItem(cAction1));
        menu.add(new JMenuItem(cAction2));
        menu.addSeparator();
        menu.add(new JMenuItem(cActionClose));
        menuBar.add(menu);

        JToolBar toolBar = new JToolBar("Toolbar");
        toolBar.add(cAction1);
        toolBar.add(cAction2);
        toolBar.add(cActionClose);
        menuBar.add(toolBar);

        setJMenuBar(menuBar);


        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public JButton getButton1() {
        return button1;
    }


    public JButton getButton2() {
        return button2;
    }

    public JButton getButtonClose() {
        return buttonClose;
    }

    public JTextArea getTextArea() {
        return textArea;
    }

    public void printDebug(String message){
        if(DEBUG){
            System.out.println(message);
        }
    }


    class CustomAction extends AbstractAction {

        private String actionName;
        private Client client;

        public CustomAction(String name, Client client){
            super(name);
            this.actionName = name;
            this.client = client;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (actionName){
                case "Search" :
                    printDebug("actionEvent : button1");

                    // a jframe here isn't strictly necessary, but it makes the example a little more real
                    JFrame searchFrame = new JFrame("InputDialog Example #1");

                    // prompt the user to enter their name
                    String searchName = JOptionPane.showInputDialog(searchFrame, "What do you want to search ?");

                    String searchAnswer = client.send("search=" + searchName);
                    searchAnswer = searchAnswer.replace(';','\n');
                    textArea.append(searchAnswer);
                    break;

                case "Play" :
                    printDebug("actionEvent : button2");

                    // a jframe here isn't strictly necessary, but it makes the example a little more real
                    JFrame playFrame = new JFrame("InputDialog Example #1");

                    // prompt the user to enter their name
                    String playName = JOptionPane.showInputDialog(playFrame, "What do you want to play ?");

                    String playAnswer = client.send("play=" + playName);
                    playAnswer = playAnswer.replace(';','\n');

                    textArea.append(playAnswer);

                    break;

                case "Close" :
                    printDebug("actionEvent : buttonClose");
                    System.exit(1);
                    break;
            }
        }


    }
}

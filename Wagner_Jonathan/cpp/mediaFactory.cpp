//
// Created by Jonathan Wagner on 09/11/2016.
//

#include "mediaFactory.h"

MediaFactory::MediaFactory(){
    //mediaList = new map<string,shared_ptr<Media>>();
    //groupList = new map<string,shared_ptr<Group>>();
}

shared_ptr<Photo> MediaFactory::createPhoto(string name, string path, int latitude, int longitude) {
    shared_ptr<Photo> p(new Photo(name,path, latitude, longitude));
    mediaList[name] = p;
    return p;
}

/**
 *
 * @param name
 * @param path
 * @param duration
 * @return
 */
shared_ptr<Video> MediaFactory::createVideo(string name, string path, int duration) {
    shared_ptr<Video> v(new Video(name,path, duration));
    mediaList[name] = v;
    return v;
}

/**
 * \brief
 *
 * \param name
 * \param path
 * \param duration
 * \param listOfChapters
 * \param numberOfChapters
 * \return
 */
shared_ptr<Film> MediaFactory::createFilm(string name, string path, int duration, int * listOfChapters, unsigned int numberOfChapters) {
    shared_ptr<Film> f(new Film(name,path, duration, listOfChapters, numberOfChapters));
    mediaList[name] = f;
    return f;
}

shared_ptr< Group< shared_ptr<Media>>> MediaFactory::createGroup(string name) {
    shared_ptr<Group< shared_ptr<Media>>> g(new Group<shared_ptr<Media>>(name));
    groupList[name] = g;
    return g;
}

void MediaFactory::search(string name, ostream& output){
    auto media = mediaList.find(name);
    if(media != mediaList.end()) (media->second)->show(output);
    else if (groupList.find(name) != groupList.end()) (groupList.find(name)->second)->show(output);
    else output << " !! Nothing named: \"" << name << "\" found !!" << endl;
}

void MediaFactory::desintegrate(string name){
    auto temp = mediaList.find(name);
    if(temp != mediaList.end()){
        //remove Media from the groups
        auto it = groupList.begin();
        for (; it != groupList.end(); it++) {
            (it->second)->remove(temp->second);
        }
        // remove Media from the map
        mediaList.erase(name);
    }
    else if(groupList.find(name) != groupList.end()){
        groupList.erase(name);
    }
    else cout << " !! Nothing named: \"" << name << "\" found !!" << endl;
}

void MediaFactory::play(string name){
    auto media = mediaList.find(name);
    if(media != mediaList.end()){
        (media->second)->play();
    }
}
#include "media.h"
#include <string>
#include <iostream>
using namespace std;

Media::Media(string _name, string _path)
:
    name(_name),
    path(_path){};

void Media::show(ostream& output) const {
    output << "#####   " << name << endl;
    output << "##" << endl;
    output << "##   Location : " << path << endl;
}


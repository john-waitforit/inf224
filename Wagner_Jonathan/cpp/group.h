//
// Created by Jonathan Wagner on 18/10/2016.
//

#ifndef GROUP_H
#define GROUP_H

#include <list>
#include <iostream>
#include <memory>


template <typename T>
class Group : public std::list<T> {
protected:
    std::string name;

    /**
     * Constructor of Group
     * Is private in order to prevent someone from creating a photo without using the factory
     *
     * @param _name
     * @return
     */
    Group<T>(std::string _name) : std::list<T>() {name = _name;}
public:
    /**
     * MediaFactory is declared as friend class so that it will be the only one able to create Groups
     */
    friend class MediaFactory;

    /**
     * Destructor of Group, helps the developper check when a group is deleted
     */
    virtual ~Group() {cout << " ~ " << name << " (Group) is being deleted ..." << endl;}

    /**
     * Getter for the name attribute
     * @return
     */
    std::string getName() const {return name;}

    /**
     * Method that will call the show function of every member of the group
     *
     * @param output
     */
    void show(ostream& output) const {
        output << "Displaying members of " << name << std::endl << std::endl;
        auto it = this->begin();
        for (; it != this->end(); ++it) {
            //(*it) is a Media *
            auto tempMedia = dynamic_pointer_cast<Media>(*it);
            if (tempMedia) tempMedia->show(output);
        }
    }
};


#endif //GROUP_H

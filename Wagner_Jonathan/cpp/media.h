#ifndef MEDIA_H
#define MEDIA_H

#include <string>
#include <iostream>
using namespace std;

// Container for all file types (photos, videos, movies, music...)

class Media{
protected:
    string name, path;

public:
    /**
     * Constructor of Media
     * Isn't actually used because Media is an abstract class
     * @return
     */
    Media() {}

    /**
     * Constructor of Media
     * Isn't used either because Media is an abstract class
     *
     * @param name
     * @param path
     * @return
     */
    Media(string name, string path);

    /**
     * Destructor for the media class, allows the developper to check if elements are deleted
     */
    virtual ~Media() {cout << " ~ " << name << " (Media) is being deleted ..." << endl;}

    /**
     *  Getter for the name attribute
     *
     * @return
     */
    string getName() const {return name;}

    /**
     *  Getter for the path attribute
     *
     * @return
     */
    string getPath() const {return path;}

    /**
     * Setter for the name attribute
     *
     * @param _name
     */
    void setName(string _name) {name = _name;}

    /**
     * Setter for the path attribute
     *
     * @param _path
     */
    void setPath(string _path) {path = _path;}

    /**
     * Method that shows (in a nice way) the attributes of the object in the output given as parameter
     * Kind of like a toString() method in Java
     *
     * @param output
     */
    virtual void show(ostream& output) const;

    /**
     * Method used to display the real media on screen, will be redefined for photos and videos
     */
    virtual void play() const = 0;    //abstract method
};

#endif // MEDIA_H

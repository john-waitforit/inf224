//
// Created by Jonathan Wagner on 11/11/2016.
//


#include <string>
#include <iostream>
#include "media.h"
#include "photo.h"


using namespace std;

void Photo::show(ostream& output) const {
    Media::show(output);
    output << "##   Size     : " << latitude << "x" << longitude << endl;
    output << "##   Kind     : Photo" << endl << endl;
}
#ifndef VIDEO_H
#define VIDEO_H

#include <string>
#include <iostream>
#include "media.h"
#include "video.h"

class Video : public Media {
protected:
    int duration;

    /**
     * Constructor of Video
     * Is protected in order to prevent someone from creating a video without using the factory
     *
     * @return
     */
    Video() : Media() {duration = 0 ;}

    /**
     * Constructor of Video
     * Is protected in order to prevent someone from creating a video without using the factory
     *
     * @param name
     * @param path
     * @param _duration
     * @return
     */
    Video(string name, string path, int _duration) : Media(name, path) {
        duration = _duration;
    }
public:

    /**
     * MediaFactory is declared as friend class so that it can create Videos and access its attributes
     */
    friend class MediaFactory;

    /**
     * Destructor for the Video class, allows the developper to check if elements are deleted
     */
    virtual ~Video() {cout << " ~ " << name << " (Video) is being deleted ..." << endl;}

    /**
     * Getter for the duration attribute
     *
     * @return
     */
    int getDuration() const {return duration;}

    /**
     * Setter for the duration attribute
     *
     * @param _duration
     */
    void setDuration(int _duration) {duration = _duration;}

    /**
     * Method that shows (in a nice way) the attributes of the object in the output given as parameter
     * Kind of like a toString() method in Java
     *
     * @param output
     */
    virtual void show(ostream& output) const override ;

    /**
     * Method used to display the real video on screen
     * Play is implemented for Mac OS, if you are using linux, change "open -a VLC " to "mvp "
     */
    virtual void play() const override {string  s = "open -a VLC \"" + path + "\"&"; system(s.c_str());}

};


#endif // VIDEO_H

//
// Created by Jonathan Wagner on 09/11/2016.
//

#ifndef INF224_MEDIAFACTORY_H
#define INF224_MEDIAFACTORY_H

#include <map>
#include "media.h"
#include "photo.h"
#include "video.h"
#include "film.h"
#include "group.h"
#include <memory>

using namespace std;

class MediaFactory {
private :

	/**
	 * Contains all the media ever created
	 * Each one is mapped to its name for search purposes
	 */
    map<string,shared_ptr<Media>> mediaList;

	/**
	 * Contains all the groups ever created
	 * Each one is mapped to its name for search purposes
	 */
    map<string,shared_ptr< Group< shared_ptr<Media>>>> groupList;
public:

    /**
     * Constructor of the mediaFactory,
	 * No arguments needed
	 *
     * @return
     */
    MediaFactory();

    /**
     * Creates a Photo and adds it to the mediaList map
	 *
     * @param name
     * @param path
     * @param latitude
     * @param longitude
     * @return
     */
    shared_ptr<Photo> createPhoto(string name, string path, int latitude, int longitude);

    /**
     * Creates a Video and adds it to the mediaList map
	 *
     * @param name
     * @param path
     * @param duration
     * @return
     */
    shared_ptr<Video> createVideo(string name, string path, int duration);

    /**
     * Creates a Film and adds it to the mediaList map
	 *
     * @param name
     * @param path
     * @param duration
     * @param listOfChapters
     * @param numberOfChapters
     * @return
     */
    shared_ptr<Film>  createFilm(string name, string path, int duration, int * listOfChapters, unsigned int numberOfChapters);

    /**
     * Creates a group and adds it to the groupList map
	 *
     * @param name
     * @return
     */
    shared_ptr< Group< shared_ptr<Media>>> createGroup(string name);

    /**
     * Performs a research in the mediaList and groupList
	 * If a result is found, displays the attributes of the object (thanks to the show method)
	 * If no result is found, it says so in the output
	 *
     * @param name
     * @param output
     */
    void search(string name, ostream& output);

    /**
     * Perform a research in the mediaList and the grouList
	 * If the object is a media, it is removed from every group it belongs
	 * then it removes it from the mediaList
	 *
	 * If the object is a group, it is removed from the groupList
	 *
     * @param name
     */
    void desintegrate(string name);

    /**
     * Performs a research in the mediaList
	 * If the media is found, it is played
	 * Else it says it wasn't found
	 *
     * @param name
     */
    void play(string name);

};


#endif //INF224_MEDIAFACTORY_H

#include <sstream>
#include <string>
#include <iostream>
#include <memory>
#include <algorithm>

#include "media.h"
#include "photo.h"
#include "video.h"
#include "film.h"
#include "main.h"
#include "group.h"
#include "mediaFactory.h"
#include "tcpserver.h"

using namespace std;
using namespace cppu;

const int PORT = 3331;

typedef shared_ptr<Group<shared_ptr<Media>>> GroupPTR;
typedef shared_ptr<Media> MediaPTR;
typedef shared_ptr<Photo> PhotoPTR;
typedef shared_ptr<Video> VideoPTR;
typedef shared_ptr<Film> FilmPTR;

class MyBase {
    MediaFactory * factory;
public:
    MyBase(MediaFactory * _factory){factory = _factory;};
    bool processRequest(TCPConnection& cnx, const string& request, string& response){

        cout << "Request received: " << request << endl;

        int pos = request.find("=");
        string parameters = request;
        string verb = request.substr(0,pos);
        parameters.erase(0,pos + 1);

        if(verb.compare("search") == 0){
            stringstream streamResponse;
            factory->search(parameters,streamResponse);
            string responseTemp = streamResponse.str();
            cout << responseTemp << endl;
            replace(responseTemp.begin(),responseTemp.end(), '\n',';');
            response = responseTemp;
        }
        else if(verb.compare("play") == 0){
            stringstream out;
            factory->search(parameters,out);
            if(out.str().compare(" !! Nothing named: \"" + parameters + "\" found !!\n") != 0){
                factory->play(parameters);
                response = "Playing " + parameters + "...";
                cout << response << endl;
            }
            else{
                response = "Did not find " + parameters + "...";
                cout << response << endl;
            }
        }
        else {
            response = "Request wasn't a known";
            cout << response << endl;
        }

        return true;
    }
};

int main(int argc, char* argv[]) {

    //testShowMedia();
    //testYankFilm(film1);
    //testGroups();
    //testFactory();
    //testString();

    MediaFactory * factory = new MediaFactory();

    initializeDatabase(factory);

    // cree le TCPServer
    shared_ptr<TCPServer> server(new TCPServer());

    // cree l'objet qui gère les données
    shared_ptr<MyBase> base(new MyBase(factory));

    // le serveur appelera cette méthode chaque fois qu'il y a une requête
    server->setCallback(*base, &MyBase::processRequest);

    // lance la boucle infinie du serveur
    cout << "Starting Server on port " << PORT << endl;
    int status = server->run(PORT);

    // en cas d'erreur
    if (status < 0) {
        cerr << "Could not start Server on port " << PORT << endl;
        return 1;
    }

    return 0;

}

void initializeDatabase(MediaFactory * factory){
    auto g = factory->createGroup("johnsGroup");

    g->push_back(factory->createPhoto("Cat Picture","/Users/john/Pictures/cat.png",180,90));
    g->push_back(factory->createFilm("Captain America", "/Users/john/Movies/CaptainAmerica.mp4", 60, new int[5] {10,11,12,13,14}, 5));

    g->push_back(factory->createFilm("Gotham S03E07", "/Users/john/Series/Gotham/Gotham - Season 3/Gotham - S03E07 - [720p] (HEVC) - Mad City Red Queen.mkv", 60, new int[5] {10,11,12,13,14}, 5));

    g->push_back(factory->createPhoto("Dog Picture","/Users/john/Pictures/dog.png",360,180));



    auto GoT = factory->createGroup("GoT");
    GoT->push_back(factory->createVideo("GoT S01E01", "/Users/john/Series/GoT S01E01.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E02", "/Users/john/Series/GoT S01E02.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E03", "/Users/john/Series/GoT S01E03.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E04", "/Users/john/Series/GoT S01E04.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E05", "/Users/john/Series/GoT S01E05.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E06", "/Users/john/Series/GoT S01E06.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E07", "/Users/john/Series/GoT S01E07.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E08", "/Users/john/Series/GoT S01E08.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E09", "/Users/john/Series/GoT S01E09.mp4", 60));
    GoT->push_back(factory->createVideo("GoT S01E10", "/Users/john/Series/GoT S01E10.mp4", 60));


    auto xMasHolidays = factory->createGroup("Christmas");
    xMasHolidays->push_back(factory->createPhoto("Tree","/Users/john/Pictures/tree.png",1920,1080));
    xMasHolidays->push_back(factory->createPhoto("Family","/Users/john/Pictures/family.png",1920,1080));
    xMasHolidays->push_back(factory->createPhoto("Gifts","/Users/john/Pictures/gifts.png",1920,1080));
    xMasHolidays->push_back(factory->createPhoto("Dog","/Users/john/Pictures/dog.png",1920,1080));

}

void testString(){

    stringstream request;
    request << "search" << ' ' <<  "cat";
    string word1;
    string word2;
    request >> word1 >> word2;

    cout << "word1: " << word1 << endl;
    cout << request.str() << endl;
    cout << "word2: " << word2 << endl;

}

void testFactory(){
    MediaFactory * factory = new MediaFactory();
    auto p = factory->createPhoto("Cat Picture","/Users/john/Pictures/cat.png",180,90);
    auto g = factory->createGroup("johnsGroup");
    auto f = factory->createFilm("Captain America", "/Users/john/Movies/CaptainAmerica.mp4", 60, new int[5] {10,11,12,13,14}, 5);

    g->push_back(p);
    g->push_back(f);
    g->push_back(factory->createPhoto("Dog Picture","/Users/john/Pictures/dog.png",360,180));

    factory->search("johnsGroup",cout);
    factory->desintegrate("Dog Picture");
    factory->desintegrate("johnsGroup");
    factory->search("Captain America",cout);


}

void testGroups() {
    cout << "Beginning of test groups..." << endl << endl;
    /*
    {
        Group<shared_ptr<Media>> group1("groupOne");

        auto photo2 = make_shared<Photo>("Dog picture", "/Users/john/Pictures/Filou.jpeg", 150, 110);

        int *listOfChapter = new int[5]{20, 21, 20, 19, 22};
        auto film1 = make_shared<Film>("Captain America", "/Users/john/Movies/CaptainAmerica.mp4", 60, listOfChapter, 5);
        delete[] listOfChapter;
        auto film2 = make_shared<Film>("Hancock", "/Users/john/Movies/CaptainAmerica.mp4", 60, listOfChapter, 5);

        auto video1 = make_shared<Video>("Cats Fighting", "/Users/john/Videos/Cats_Fighting", 3);

        group1.push_back(make_shared<Film>("The Notebook","/Users/john/Movies/The_Notebook.mp4",60,listOfChapter,5));
        group1.push_back(photo2);
        group1.push_back(film1);
        group1.push_back(video1);
        group1.show(std::cout);

        group1.pop_front();


        cout << endl << "Destroying group1" << endl;
    }

    auto photo1 = make_shared<Photo>("Menu 5 Diamants", "/Users/john/Pictures/PizzeriaCinqDiamants.jpeg", 10, 10);

    Group<MediaPTR> group2("groupPhotos");
    group2.push_back(photo1);
    group2.push_back(photo1);
    group2.push_back(photo1);
    group2.push_back(photo1);
    group2.show(std::cout);
*/
    cout << "End of test groups" << endl;
    cout << endl;
}

/**
 * Goes through a list of Media and shows them
 *
 * @param tableMedia
 * @param count
 * @param output
 */
void showMedias(Media ** tableMedia, unsigned int count, ostream& output){
    for (unsigned int k = 0; k < count; ++k) {
        output << "## Media n° : " << k << endl;
        tableMedia[k]->show(output);
        output << endl;
    }
}

void testShowMedia(){
    cout << "Beginning of test show media..." << endl << endl;
    Media ** medias = new Media * [10];
    unsigned int count = 0;

    //medias[count++] = new Photo("Menu 5 Diamants", "/Users/john/Pictures/PizzeriaCinqDiamants.jpeg", 10, 10);
    //medias[count++] = new Video("Captain America", "/Users/john/Movies/CaptainAmerica.mp4", 60);
    showMedias(medias, count, cout);
    cout << "End of test show media" << endl;
    cout << endl;
}

void testYankFilm(const Film * film1){
    cout << "Beginning of test yank film..." << endl << endl;
    int * listOfChapter = new int [5];
    listOfChapter[0]= 20;
    listOfChapter[1]= 21;
    listOfChapter[2]= 20;
    listOfChapter[3]= 19;
    listOfChapter[4]= 22;
    film1->printChapters(cout);

    cout << "Copying first film..." << endl;

    //Film * film2 = new Film(* film1);
    listOfChapter[3]= 100;
    //film2->setChapters(listOfChapter,5);
    delete[] listOfChapter;

    //film2->show(cout);

    cout << "End of test yank film" << endl;
    cout << endl;
}

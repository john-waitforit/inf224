#ifndef PHOTO_H
#define PHOTO_H

#include <string>
#include <iostream>

using namespace std;

class Photo : public Media {
private:
    int latitude, longitude;

    /**
     * Constructor of Photo
     * Is private in order to prevent someone from creating a photo without using the factory
     *
     * @return
     */
    Photo() : Media() {latitude = 0 ; longitude = 0;}

    /**
     * Constructor of Photo
     * Is private in order to prevent someone from creating a photo without using the factory
     *
     * @param name
     * @param path
     * @param _latitude
     * @param _longitude
     * @return
     */
    Photo(string name, string path, int _latitude, int _longitude) : Media(name, path) {
        latitude = _latitude;
        longitude = _longitude;
    }
public:

    /**
         * MediaFactory is declared as friend class so that it will be the only one able to create Photos
     */
    friend class MediaFactory;

    /**
     * Destructor of Photo, helps the developper check when a photo is deleted
     */
    virtual ~Photo() {cout << " ~ " << name << " (Photo) is being deleted ..." << endl;}

    /**
     * Getter for the latitude attribute
	 *
     * @return
     */
    int getLatitude() const {return latitude;}
    
	/**
     * Getter for the longitude attribute
     * @return
     */
    int getLongitude() const {return longitude;}

    /**
     * Setter for the latitude attribute
	 *
     * @param _latitude
     */
    void setLatitude(int _latitude) {latitude = _latitude;}

    /**
     * Setter for the longitude attribute 
	 *
     * @param _longitude
     */
    void setLongitude(int _longitude) {longitude = _longitude;}

    /**
     * Method that shows (in a nice way) the attributes of the object in the output given as parameter
	 * Kind of like a toString() method in Java
	 *
     * @param output
     */
    virtual void show(ostream& output) const override;

    /**
     * Method used to display the real photo on screen
     * Play is implemented for Mac OS, if you are using linux, change "open -a Preview " to "imagej "
	 */
    virtual void play() const override {string  s = "open -a Preview " + path + "&"; system(s.c_str());}

};

#endif // PHOTO_H

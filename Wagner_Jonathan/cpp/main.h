#ifndef MAIN_H
#define MAIN_H

#include "media.h"
#include "tcpserver.h"
#include "mediaFactory.h"
#include <string>
#include <iostream>

using namespace cppu;
using namespace std;

/**
 * Function for the beginning of the project to display some items
 * @param tableMedia
 * @param count
 * @param output
 */
void showMedias(Media ** tableMedia, int count, ostream& output);

/**
 * Function to test the showmedias method
 */
void testShowMedia();

/**
 * Function to try to create a copy of a film
 * @param film1
 */
void testYankFilm(const Film * film1);

/**
 * Function to test the imprementation of the group class
 */
void testGroups();

/**
 * Function to test the implementation of the Factory class
 */
void testFactory();

/**
 * Function to understand how strings and streamstrings behave
 */
void testString();

/**
 * Function called every time the client sends a request to the server.
 * The request can be a search query or a play query.
 * The output is true if everything went as planned
 *
 * @param cnx
 * @param request
 * @param response
 * @return
 */
bool processRequest(TCPConnection& cnx, const string& request, string& response);

/**
 * Function that creates sme objects in the "database"
 *
 * @param factory
 */
void initializeDatabase(MediaFactory * factory);

#endif // MAIN_H

#include "film.h"
#include <string>
#include <iostream>
using namespace std;

Film::Film(string name, string path, int duration, const int * _chapters, unsigned int _numberOfChapters) : Video(name, path, duration){
    chapters = new int [_numberOfChapters];
    for(unsigned int i = 0; i < _numberOfChapters; i++){
        chapters[i] = _chapters[i];
    }
    numberOfChapters = _numberOfChapters;
}

Film::Film(const Film & f) : Video (f){
    setChapters(f.chapters,f.numberOfChapters);
}

Film& Film::operator=(const Film & f) {
    Video::operator=(f);
    setChapters(f.chapters,f.numberOfChapters);
    return * this;
}

void Film::setChapters(const int * _chapters, unsigned int _numberOfChapters){
    delete[] chapters;  //Clean things up before writing
    numberOfChapters = _numberOfChapters;
    if(numberOfChapters != 0) {
        chapters = new int[numberOfChapters];
        for (unsigned int i = 0; i < numberOfChapters; i++) {
            chapters[i] = _chapters[i];
        }
    }
    else { //Error message
        cout << "Film::setChapters was called with a numberOfChapters equal to 0" << endl;
    }
}

void Film::printChapters(ostream& output) const {
    output << "##   Chapters : " << endl;
    for (unsigned int k = 1; k < numberOfChapters + 1; ++k) {
        // k represents the 'normal' way of counting
        output << "##     - " << k << " (" << chapters[k - 1] << " min)" << endl;
    }
    output << endl;
}

void Film::show(ostream& output) const {
    Video::show(output);
    output << "##   Kind     : Film" << endl;
    printChapters(output);
}


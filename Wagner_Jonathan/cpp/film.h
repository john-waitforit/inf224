#ifndef FILM_H
#define FILM_H

#include <string>
#include <iostream>
#include "video.h"
using namespace std;

class Film : public Video {
protected:
    int * chapters;
    unsigned int numberOfChapters;

    /**
	 * Constructor of Film
	 * Is protected in order to prevent someone from creating a film without using the factory
     *
     * @param name
     * @param path
     * @param duration
     * @param _chapters
     * @param _numberOfChapters
     * @return
     */
    Film(string name, string path, int duration, const int * _chapters, unsigned int _numberOfChapters);

    /**
     * Constructor of film that copies another film
	 *
     * @param f
     * @return
     */
    Film(const Film & f);
public:

    /**
     * MediaFactory is declared as friend class so that it can create Film and access its attributes
     */
    friend class MediaFactory;

    /**
     * Destructor for the film class, allows the developper to check if elements are deleted
     */
    virtual ~Film() {delete[] chapters;cout << " ~ " << name << " (Film) is being deleted ..." << endl;}

    /**
     * Redefinition of the equal operator, allows to create a new film which is a copy
	 * of the other one with the equal operator
	 *
     * @param f
     * @return
     */
    Film& operator=(const Film & f);

    /**
     * Setter for the list of chapters
	 *
     * @param _chapters
     * @param _numberOfChapters
     */
    virtual void setChapters(const int * _chapters, unsigned int _numberOfChapters);

    /**
     * Getter for the list of chapters
	 *
     * @return
     */
    const int * getChapters() const {return chapters;}

    /**
     * Getter for the number of chapters
	 *
     * @return
     */
    unsigned int getNumberOfChapters() const {return numberOfChapters;}

    /**
     * Method that displays the list of chapters with their duration
	 * Uses the output given as parameters
	 * 
     * @param output
     */
    virtual void printChapters(ostream& output) const;
    
	/**
     * Method that shows (in a nice way) the attributes of the object in the output given as parameter
	 * Kind of like the toString() methode in Java
	 *
     * @param output
     */
    virtual void show(ostream& output) const override ;

};

#endif // FILM_H

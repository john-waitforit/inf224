#include "media.h"
#include "video.h"
#include <string>
#include <iostream>
using namespace std;

void Video::show (ostream& output) const {
    Media::show(output);
    output << "##   Duration : " << duration << endl;
}
